
## Asystence Panel Web

Asystence es un proyecto generado para la clase de Sistemas Virtuales de la UPSLP,
el cual tiene la funcionalidad principal de gestionar y realizar las encuestas o
check list que se lleva a acabo por parte de los revisores de cada edificio de la 
instutución.

El sistema se confirma de 3 elementos para su funcionamiento completo:
- Servidor API (Backend).
- Panel Web (Frontend).
- App Movil (Frontend).

Es fundamental tener instalado el servidor API y redireccionar las peticiones hacia el mismo.

Para poder hacer uso del sistema de Asystence Panel Web de una manera local,
es necesario tener instalado Angular CLI y Node JS.

## Instalacion de Node Js

Descarague el instalador desde el sitio oficial.

https://nodejs.org/es/


## Instalacion de Angular

``` bash
npm install -g @angular/cli
```

## Instalacion de Proyecto

``` bash
 cd asystence
```

- Instalación de dependencias.

``` bash
 npm install 
```

- Inicar el servidor de Angular

``` bash
 ng serve
```

- Desplegar Proyecto 

``` bash
 ng build --prod
```
Se generara un directorio de nombre "dist" en la raiz del proyecto, la cual se podra poner en un 
un direcctorio de algun servidor utilizando apache o nginx.

## Navegadores Soportados

<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/chrome-logo.png?raw=true" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/firefox-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/edge-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/safari-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/opera-logo.png" width="64" height="64">

