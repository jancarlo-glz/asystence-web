import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://18.222.150.29/api/';
  //url = 'http://localhost:80/api/';

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar) { }


  get(): any {
    return this.http.get(this.url + 'edificios');
  }

  login(email, password): any {
    const body = {
      email: email,
      password: password
    };
    return this.http.post(this.url + 'auth/login', body);
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }
}
