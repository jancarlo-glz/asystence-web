import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  email: any;
  password: any;
  loginForm: FormGroup;
  constructor(private _loginService: LoginService,
              private router: Router,
              private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  login(): void {
    if (this.loginForm.value.username && this.loginForm.value.password) {
      this._loginService.login(this.loginForm.value.username, this.loginForm.value.password)
      .subscribe((resp: any) => {
        if (resp.status && resp.session === 'Admin') {
          localStorage.setItem('_user', JSON.stringify(this.loginForm.value.username));
          this._loginService.showSanckBar('¡Bienvenido!');
          this.router.navigate(['/buildings']);
        }
      }, (err: any) => {
        this._loginService.showSanckBar('¡Upss! ' + err.errors);
      });
    } else {
      this._loginService.showSanckBar('Introduce los datos');
    }
  }

  ngOnDestroy() {
  }

}
