import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AddEditBuildingsComponent } from './asystence/buildings/add-edit-buildings/add-edit-buildings.component';
import { ConfirmDeletedComponent } from './asystence/confirm-deleted/confirm-deleted.component';
import { BrowserModule } from '@angular/platform-browser';
import { BuildingsService } from './asystence/buildings/buildings.service';
import { MatSnackBarModule, MatDialogModule } from '@angular/material';
import { ClassroomByBuildingsComponent } from './asystence/buildings/classroom-by-buildings/classroom-by-buildings.component';
import { AddEditClassroomComponent } from './asystence/buildings/classroom-by-buildings/add-edit-classroom/add-edit-classroom.component';
import { ClassroomService } from './asystence/buildings/classroom-by-buildings/classroom.service';
import { StaffComponent } from './asystence/staff/staff.component';
import { SubjectsComponent } from './asystence/subjects/subjects.component';
import { TeachersComponent } from './asystence/teachers/teachers.component';
import { SemesterComponent } from './asystence/semester/semester.component';
import { QuestionnairesComponent } from './asystence/questionnaires/questionnaires.component';
import { QuestionsComponent } from './asystence/questions/questions.component';
import { ScheduleComponent } from './asystence/schedule/schedule.component';
import { AddEditStaffComponent } from './asystence/staff/add-edit-staff/add-edit-staff.component';
import { StaffService } from './asystence/staff/staff.service';
import { SetSubjectsTeacherComponent } from './asystence/staff/set-subjects-teacher/set-subjects-teacher.component';
import { SetBuildingStaffComponent } from './asystence/staff/set-building-staff/set-building-staff.component';
import { AddEditSemesterComponent } from './asystence/semester/add-edit-semester/add-edit-semester.component';
import { SemesterService } from './asystence/semester/semester.service';
import { AddEditSubjectsComponent } from './asystence/subjects/add-edit-subjects/add-edit-subjects.component';
import { SubjectsService } from './asystence/subjects/subjects.service';
import { AddEditTeachersComponent } from './asystence/teachers/add-edit-teachers/add-edit-teachers.component';
import { LoginService } from './pages/login/login.service';
import { ShowSubjectsComponent } from './asystence/subjects/show-subjects/show-subjects.component';
import { TeachersService } from './asystence/teachers/teachers.service';
import { AddEditQuestionnariesComponent } from './asystence/questionnaires/add-edit-questionnaries/add-edit-questionnaries.component';
import { QuestionnariesService } from './asystence/questionnaires/questionnaries.service';
import { QuestionsService } from './asystence/questions/questions.service';
import { AddEditScheduleComponent } from './asystence/schedule/add-edit-schedule/add-edit-schedule.component';
import { ScheduleService } from './asystence/schedule/schedule.service';
import { AnswersComponent } from './asystence/answers/answers.component';
import { ShowAnswersComponent } from './asystence/answers/show-answers/show-answers.component';
import { AnswersService } from './asystence/answers/answers.service';
import { MessageExceptionsComponent } from './asystence/message-exceptions/message-exceptions.component';

 
@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    MatSnackBarModule,
    AppRoutingModule,
    MatDialogModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    SetSubjectsTeacherComponent,
    MessageExceptionsComponent
  ],
  providers: [BuildingsService, ClassroomService, StaffService, SemesterService, SubjectsService,
              LoginService, TeachersService, QuestionnariesService, QuestionsService, ScheduleService,
              AnswersService],
  bootstrap: [AppComponent],
  entryComponents: [MessageExceptionsComponent ]
})
export class AppModule { }
