import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {
  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';


  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  // getAllQuestions(): any {
  //   return this.http.get(this.url + 'preguntas');
  // }

  getQuestionsById(id): any {
    return this.http.get(this.url + 'cuestionarios/' + id + '/edit');
  }

  saveQuestions(formData, id): any {
    const body  = {
      nombre: formData.value.name,
      tipo: formData.value.type,
      opciones: formData.value.options
    };
    return this.http.post(this.url + 'cuestionarios/' + id + '/storeQ', body);
  }

  editQuestions(id, formData, id_question): any {
    const body  = {
      nombre: formData.value.name,
      tipo: formData.value.type,
      opciones: formData.options
    };
    return this.http.put(this.url + 'cuestionarios/' + id + '/' + id_question, body);
  }

  deleteQuestions(id, id_question): any {
    return this.http.delete(this.url + 'cuestionarios/' + id + '/' + id_question + '/destroyQ');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    const dialogRef = this.dialog.open(MessageExceptionsComponent, {
      width: '450px',
      data: {
        message: message.error
      }
    });
  }

}
