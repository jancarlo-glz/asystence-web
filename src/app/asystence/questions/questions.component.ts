import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { QuestionsService } from './questions.service';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {
  angForm: FormGroup;
  id_questionnarie: any;
  questionnarie_name: any;
  questionsList = [
    { id: 1, nombre: '¿Estado del Salon?', tipo: 'radio', opciones: [ {name: 'sucio'}, { name: 'limpio'}]},
    { id: 2, nombre: '¿Orden en Clase?', tipo: 'checkbox',
    opciones: [
                {name: 'Desorden'}, { name: 'Orden'}, {name: 'Atentos'}, {name: 'Video'}
              ]
    },
    { id: 3, nombre: 'Observaciones', tipo: 'libre', opciones: []},
  ];
  id_question: any;
  typeList = [
    { value: 'libre', name: 'Libre'},
    { value: 'radio', name: 'Una Respuesta'},
    { value: 'checkbox', name: 'Muchas Respuestas'},
  ];
  type: any;
  constructor(private fb: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private _questionsService: QuestionsService) {
      this.type = 'add';
      const params: any = this.route.params;
      console.log('Paramas!!! ' + JSON.stringify(params));
      this.id_questionnarie = params._value.id;
      console.log('ID!!!' + this.id_questionnarie);
      
      this.createForm();
      this.getQuestionsByQuestionnaries();
     }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      type: ['', Validators.required],
      options: this.fb.array([this.initItemRows()])
    });
  }

  changeToSave(): void {
    this.type = 'edit';
    this.angForm.reset();
  }

  initItemRow(nombre: any): any {
    return this.fb.group({
        // list all your form controls here, which belongs to your form array
        nombre: [nombre],
    });
  }

  initItemRows(): any {
      return this.fb.group({
          // list all your form controls here, which belongs to your form array
          nombre: [''],
      });
  }

  addNewRow(): any {
      // control refers to your formarray
      const control = <FormArray>this.angForm.controls['options'];
      // add new formgroup
      control.push(this.initItemRows());
  }


  deleteRow(index: number): any {
      // control refers to your formarray
      const control = <FormArray>this.angForm.controls['options'];
      // remove the chosen row
      control.removeAt(index);
  }

  getInfoQuestion(id, name, type, options): void {
    // let control = [];
    this.deleteRow(0);
    this.type = 'edit';
    this.id_question = id;
    this.angForm.controls.name.setValue(name);
    this.angForm.controls.type.setValue(type);
    options.forEach(element => {
      console.log('Element ' + element.name);
      const control = <FormArray>this.angForm.controls['options'];
      control.push(this.initItemRow(element.nombre));
    });
    // for (let i = 0; i < options.length; ++i) {
    //    const control = <FormArray>this.angForm.controls['items'];
    //   control.push(this.initItemRow(options[i].name));
    // }
  }

  getQuestionsByQuestionnaries(): void {
    this._questionsService.getQuestionsById(this.id_questionnarie)
      .subscribe((res: any) => {
        this.questionsList = res.preguntas;
        console.log('Preguntas ' + res.preguntas);
      }, (err: any) => {
        this._questionsService.showMessageExcep(err);
    });
  }

  saveQuestions(): void {
    this._questionsService.saveQuestions(this.angForm, this.id_questionnarie)
      .subscribe((res: any) => {
        this._questionsService.showSanckBar('¡Datos Guardados!');
        this.getQuestionsByQuestionnaries();
        this.angForm.reset();
      }, (err: any) => {
        this._questionsService.showMessageExcep(err);
    });
  }

  editQuestions(): void {
    this._questionsService.editQuestions(this.id_questionnarie, this.angForm, this.id_question)
      .subscribe((res: any) => {
        this._questionsService.showSanckBar('¡Datos Guardados!');
        this.getQuestionsByQuestionnaries();
      }, (err: any) => {
        this._questionsService.showMessageExcep(err);
    });
  }

  deleteQuestions(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._questionsService.deleteQuestions(this.id_questionnarie, id)
          .subscribe(arg => {
            this._questionsService.showSanckBar('¡Datos Eliminados!');
            this.getQuestionsByQuestionnaries();
          }, (err: any) => {
            this._questionsService.showMessageExcep(err);
          });
      }
    });
  }

}
