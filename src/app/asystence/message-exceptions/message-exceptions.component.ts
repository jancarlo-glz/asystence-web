import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddEditBuildingsComponent } from '../buildings/add-edit-buildings/add-edit-buildings.component';
 
@Component({
  selector: 'app-message-exceptions',
  templateUrl: './message-exceptions.component.html',
  styleUrls: ['./message-exceptions.component.scss']
})
export class MessageExceptionsComponent implements OnInit {
message: any;
  constructor(public dialogRef: MatDialogRef<MessageExceptionsComponent>,
              @Inject(MAT_DIALOG_DATA) public _data) {
                this.message = _data.message;
               }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

}
