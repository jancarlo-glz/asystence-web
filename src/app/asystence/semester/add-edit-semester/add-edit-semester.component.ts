import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SemesterService } from '../semester.service';

@Component({
  selector: 'app-add-edit-semester',
  templateUrl: './add-edit-semester.component.html',
  styleUrls: ['./add-edit-semester.component.scss']
})
export class AddEditSemesterComponent implements OnInit {
  type: any;
  id_semester: any;
  angForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddEditSemesterComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _semesterService: SemesterService) {
                this.type = _data.type;
                if (this.type === 'edit') {
                this.id_semester = _data.id;
                this.getSemesterById();
                }
                this.createForm();
              }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ]
    });
  }

  getSemesterById(): void {
    this._semesterService.getSemesterById(this.id_semester)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.nombre);

      }, (err: any) => {
        this._semesterService.showMessageExcep(err);
      });
  }

  addSemester(): void {
    this._semesterService.saveSemester(this.angForm)
      .subscribe((res: any) => {
        this._semesterService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._semesterService.showMessageExcep(err);
      });
  }

  editSemester(): void {
    this._semesterService.editSemester(this.id_semester, this.angForm)
      .subscribe((res: any) => {
        this.dialogRef.close();
        this._semesterService.showSanckBar('¡Datos Agregados!');
      }, (err: any) => {
        this._semesterService.showMessageExcep(err);
      });
  }

}
