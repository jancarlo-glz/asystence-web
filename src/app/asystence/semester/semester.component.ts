import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SemesterService } from './semester.service';
import { AddEditSemesterComponent } from './add-edit-semester/add-edit-semester.component';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';

@Component({
  selector: 'app-semester',
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.scss']
})
export class SemesterComponent implements OnInit {
  semesterList: any = [
    { id: '1', name: 'VP-UPSLP17' },
    { id: '2', name: 'OI-UPSLP17' },
    { id: '3', name: 'VP-UPSLP18' },
    { id: '4', name: 'OI-UPSLP18' },
    { id: '5', name: 'VP-UPSLP19' }
  ];
  constructor(private dialog: MatDialog,
              private _semesterService: SemesterService) {
                this.getSemesters();
              }

  ngOnInit() {
  }

  getSemesters(): void {
    this._semesterService.getAllSemesters()
      .subscribe((res: any) => {
        if (res) {
          this.semesterList = res;
        } else {
          this.semesterList = [];
        }
      }, (err: any) => {
        this._semesterService.showMessageExcep(err);
        this.semesterList = [];
      });
  }

  addSemester(): void {
    const dialogRef = this.dialog.open(AddEditSemesterComponent, {
      width: '650px',
      data: {
        type: 'add'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSemesters();
    });
  }

  editSemester(id): void {
    const dialogRef = this.dialog.open(AddEditSemesterComponent, {
      width: '650px',
      data: {
        type: 'edit',
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSemesters();
    });
  }

  deleteSemester(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '350px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._semesterService.deleteSemester(id)
          .subscribe(arg => {
            this.getSemesters();
            this._semesterService.showSanckBar('¡Datos Eliminados!');
          }, (err: any) => {
            this._semesterService.showMessageExcep(err);
          });
      }
    });
  }

}
