import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class SemesterService {

  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllSemesters(): any {
    return this.http.get(this.url + 'semestre');
  }

  getSemesterById(id): any {
    return this.http.get(this.url + 'semestre/' + id + '/edit');
  }

  saveSemester(formData): any {
    const body  = {
      nombre: formData.value.name,
    };
    return this.http.post(this.url + 'semestre/store', body);
  }

  editSemester(id, formData): any {
    const body  = {
      nombre: formData.value.name,
    };
    return this.http.put(this.url + 'semestre/' + id + '/update', body);
  }

  deleteSemester(id): any {
    return this.http.delete(this.url + 'semestre/' + id + '/destroy');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    const dialogRef = this.dialog.open(MessageExceptionsComponent, {
      width: '450px',
      data: {
        message: message.error
      }
    });
  }
}
