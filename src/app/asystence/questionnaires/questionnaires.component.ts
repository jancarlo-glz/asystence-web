import { Component, OnInit } from '@angular/core';
import { QuestionnariesService } from './questionnaries.service';
import { MatDialog } from '@angular/material';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';
import { AddEditQuestionnariesComponent } from './add-edit-questionnaries/add-edit-questionnaries.component';

@Component({
  selector: 'app-questionnaires',
  templateUrl: './questionnaires.component.html',
  styleUrls: ['./questionnaires.component.scss']
})
export class QuestionnairesComponent implements OnInit {
  questionnariesList: any;

  constructor(private _questionnarieService: QuestionnariesService,
              private dialog: MatDialog) {
                this.getQuestionnaries();
              }

  ngOnInit() {
  }

  getQuestionnaries(): void {
    this._questionnarieService.getAllQuestionnaries()
      .subscribe((res: any) => {
        this.questionnariesList = res;
      }, (err: any) => {
        this._questionnarieService.showMessageExcep(err);
      });
  }

  addQuestionnarie(): void {
    const dialogRef = this.dialog.open(AddEditQuestionnariesComponent, {
      width: '650px',
      data: {
        type: 'add'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getQuestionnaries();
    });
  }

  editQuestionnarie(id): void {
    const dialogRef = this.dialog.open(AddEditQuestionnariesComponent, {
      width: '650px',
      data: {
        type: 'edit',
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getQuestionnaries();
    });
  }

  deleteQuestionnarie(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '350px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._questionnarieService.deleteQuestionnarie(id)
          .subscribe(arg => {
            this.getQuestionnaries();
            this._questionnarieService.showSanckBar('¡Datos Eliminados!');
          }, (err: any) => {
            this._questionnarieService.showMessageExcep(err);
          });
      }
    });
  }

}
