import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class QuestionnariesService {

  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllQuestionnaries(): any {
    return this.http.get(this.url + 'cuestionarios');
  }

  getQuestionnarieById(id): any {
    return this.http.get(this.url + 'cuestionarios/' + id + '/edit');
  }

  saveQuestionnarie(formData): any {
    const body  = {
      nombre: formData.value.name,
    };
    return this.http.post(this.url + 'cuestionarios/store', body);
  }

  editQuestionnarie(id, formData): any {
    const body  = {
      nombre: formData.value.name,
    };
    return this.http.put(this.url + 'cuestionarios/' + id + '/update', body);
  }

  deleteQuestionnarie(id): any {
    return this.http.delete(this.url + 'cuestionarios/' + id + '/destroy');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    const dialogRef = this.dialog.open(MessageExceptionsComponent, {
      width: '450px',
      data: {
        message: message.error
      }
    });
  }
}
