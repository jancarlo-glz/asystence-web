import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionnariesService } from '../questionnaries.service';

@Component({
  selector: 'app-add-edit-questionnaries',
  templateUrl: './add-edit-questionnaries.component.html',
  styleUrls: ['./add-edit-questionnaries.component.scss']
})
export class AddEditQuestionnariesComponent implements OnInit {
  angForm: FormGroup;
  type: any;
  id_questionnrie: any;
  constructor(public dialogRef: MatDialogRef<AddEditQuestionnariesComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public _data,
    private _questionnariesService: QuestionnariesService) {
      this.type = _data.type;
      if (this.type === 'edit') {
      this.id_questionnrie = _data.id;
      this.getQuestionnarieById();
      }
      this.createForm();
    }


  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ]
    });
  }

  getQuestionnarieById(): void {
    this._questionnariesService.getQuestionnarieById(this.id_questionnrie)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.nombre);

      }, (err: any) => {
        this._questionnariesService.showMessageExcep(err);
      });
  }

  addQuestionnarie(): void {
    this._questionnariesService.saveQuestionnarie(this.angForm)
      .subscribe((res: any) => {
        this._questionnariesService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._questionnariesService.showMessageExcep(err);
      });
  }

  editQuestionnarie(): void {
    this._questionnariesService.editQuestionnarie(this.id_questionnrie, this.angForm)
      .subscribe((res: any) => {
        this.dialogRef.close();
        this._questionnariesService.showSanckBar('¡Datos Agregados!');
      }, (err: any) => {
        this._questionnariesService.showMessageExcep(err);
      });
  }

}
