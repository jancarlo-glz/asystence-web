import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SubjectsService } from '../subjects.service';
import { SemesterService } from '../../semester/semester.service';

@Component({
  selector: 'app-add-edit-subjects',
  templateUrl: './add-edit-subjects.component.html',
  styleUrls: ['./add-edit-subjects.component.scss']
})

export class AddEditSubjectsComponent implements OnInit {
  type: any;
  id_subjects: any;
  angForm: FormGroup;
  careerList: any;
  semestersList: any;

  constructor(public dialogRef: MatDialogRef<AddEditSubjectsComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _subjectsService: SubjectsService,
              private _semesterService: SemesterService) {
      this.type = _data.type;
      if (_data.id) {
        this.id_subjects = _data.id;
        this.getStaffById();
      }
      this.createForm();
      this.getCareers();
      this.getSemester();
     }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      career: ['', Validators.required],
      semester: ['', Validators.required ],
      credits: ['', Validators.required ]
    });
  }

  getCareers(): void {
    this._subjectsService.getCareers()
      .subscribe((res: any) => {
        this.careerList = res;
      }, (err: any) => {
        this._subjectsService.showMessageExcep(err);
      });
  }

  getSemester(): void {
    this._semesterService.getAllSemesters()
    .subscribe((res: any) => {
      this.semestersList = res;
    }, (err: any) => {
      this._subjectsService.showMessageExcep(err);
    });
  }

  getStaffById(): void {
    this._subjectsService.getSubjectsById(this.id_subjects)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.nombre);
        this.angForm.controls.role.setValue(res.career);
        this.angForm.controls.email.setValue(res.semester);
        this.angForm.controls.password.setValue(res.credits);
      }, (err: any) => {
        this._subjectsService.showMessageExcep(err);
      });
  }

  addStaff(): void {
    this._subjectsService.saveSubjects(this.angForm)
      .subscribe((res: any) => {
        this._subjectsService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._subjectsService.showMessageExcep(err);
      });
  }

  editStaff(): void {
    this._subjectsService.editSubjects(this.id_subjects, this.angForm)
      .subscribe((res: any) => {
        this._subjectsService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._subjectsService.showMessageExcep(err);
      });
  }

}
