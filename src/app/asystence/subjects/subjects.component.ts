import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SubjectsService } from './subjects.service';
import { AddEditSubjectsComponent } from './add-edit-subjects/add-edit-subjects.component';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';

export interface StaffData {
  nombre: string;
  carrera: string;
  semestre: string;
  creditos: string;
}

/** Datos de Prueba para ayudar a mostrar informacion dentro de la tabla */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'career', 'semester', 'credits', 'options'];
  dataSource: MatTableDataSource<StaffData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              private _subjectsService: SubjectsService) {
    // Create 100 users
    const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
    // this.getAllStaff();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.getAllStaff();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllSubjects(): void {
    this._subjectsService.getAllSubjects()
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err: any) => {
        this._subjectsService.showMessageExcep(err);
      });
  }

  addSubjects(): void {
    const dialogRef = this.dialog.open(AddEditSubjectsComponent, {
      width: '650px',
      data: {
        type: 'add'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllSubjects();
    });
  }

  editSubjects(id): void {
    const dialogRef = this.dialog.open(AddEditSubjectsComponent, {
      width: '650px',
      data: {
        type: 'edit',
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllSubjects();
    });
  }


  deleteSubjects(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._subjectsService.deleteSubjects(id)
          .subscribe(arg => {
            this._subjectsService.showSanckBar('¡Datos Eliminados!');
            this.getAllSubjects();
          }, (err: any) => {
            this._subjectsService.showMessageExcep(err);
          });
      }
    });
  }

}

/** Builds and returns a new User. */
function createNewUser(id: number): StaffData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    semestre: Math.round(Math.random() * 100).toString(),
    carrera: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    nombre: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    creditos: Math.round(Math.random() * 100).toString(),
  };
}
