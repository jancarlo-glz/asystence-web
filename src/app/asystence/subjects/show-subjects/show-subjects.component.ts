import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TeachersService } from '../../teachers/teachers.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-show-subjects',
  templateUrl: './show-subjects.component.html',
  styleUrls: ['./show-subjects.component.scss']
})
export class ShowSubjectsComponent implements OnInit {
  semester: any;
  career: any;
  id_teacher: any;
  subjectsHas = [];
  subjectList = [];
  selectedSubject: any;
  dataSelected: any;
  searchSubject: any;
  dataSearch: any;
  selectable = false;
  removable = true;
  constructor(public dialogRef: MatDialogRef<ShowSubjectsComponent>,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _teacherService: TeachersService) {
                this.career = _data.career;
                this.semester = _data.semester;
                this.id_teacher = _data.id_teacher;
                if (this.subjectsHas === undefined) {
                  this.subjectsHas = [];
                }
                console.log('Has!! ' + this.subjectsHas);

                this.getSubjects();
               }

  ngOnInit() {
  }

  getSubjects(): void {
    // let tempArray = [];
    this._teacherService.getSubjectsByCareerAndSemesterWithOutHas(this.career, this.semester, this.id_teacher)
      .subscribe((res: any) => {
        this.dataSearch = res;
        this.subjectList = res;
        // Object.entries(res).forEach(([key, value]) => {
        // });
        // this.subjectList = this.createList(Array(res), this.subjectsHas);
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  createList(arrayBase: any, arrayQuit: any): any {
    // let tempArray;
    // // // arrayBase.forEach((currenValue, index, array) => {
    // // //   console.log('Index ' + index);
    // // //     console.log('Value ' + currenValue);
    // // //   arrayQuit.forEach((currenValue, index, array) => {
    // // //     // if (element === element2) {
    // // //     //     tempArray = this.removeItem(arrayBase, element2);
    // // //     // }
    // // //   });
    // // // });

    // if (tempArray) {
    //   return tempArray;
    // } else {
    //   return arrayBase;
    // }
  }

  transform(value: any, args?: any[]): any[] {
    // check if 'routes' exists
    if (value) {
        // create instance vars to store keys and final output
        const keyArr: any[] = Object.keys(value),
            dataArr = [];
        // loop through the object,
        // pushing values to the return array
        keyArr.forEach((key: any) => {
            dataArr.push(value[key]);
        });
        // return the resulting array
        return dataArr;
    }
  }


  removeItem(array, item): any {
    const index = array.indexOf(item);
    if (index >= 0) {
        array.splice(index, 1);
    }
    return array;
  }

  onNgModelChange($event: any): any {
    console.log('Event ' + $event.key);
    // let item = JSON.stringify({ $event.key: $event.value });
   // console.log('Event ' + item);
    //item = { $event.key }
    this.selectedSubject = $event;
    this.dataSelected = this.selectedSubject;
    console.log('Select ' + JSON.stringify(this.dataSelected));
  }

  removeChip(index: any): void {
    // const index = this.dataSelected.indexOf(fruit);

    if (index >= 0) {
        this.dataSelected.splice(index, 1);
        this.selectedSubject = this.dataSelected;
    }

  }

  search(): any {
    const data = this.searchSubject;
    if (data.length > 0) {
        this.dataSearch = this.subjectList.filter(
            key => key.value.includes(this.searchSubject));
    } else {
        this.dataSearch = this.subjectList;
    }
}

}
