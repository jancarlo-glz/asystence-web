import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  url = 'http://18.222.150.29/api/';

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllSubjects(): any {
    return this.http.get(this.url + 'materias');
  }

  getSubjectsById(id): any {
    return this.http.get(this.url + 'materias/' + id + '/edit');
  }

  getCareers(): any {
    return this.http.get(this.url + 'getCarreras');
  }

  saveSubjects(formData): any {
    const body  = {
      nombre: formData.value.name,
      creditos: formData.value.credits,
      semestre_id: formData.value.semester,
      carrera_id: formData.value.career,
    };
    return this.http.post(this.url + 'materias/store', body);
  }

  editSubjects(id, formData): any {
    const body  = {
      nombre: formData.value.name,
      creditos: formData.value.credits,
      semestre_id: formData.value.semester,
      carrera_id: formData.value.career,
    };
    return this.http.put(this.url + 'materias/' + id + '/update', body);
  }

  deleteSubjects(id): any {
    return this.http.delete(this.url + 'materias/' + id + '/destroy');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    const dialogRef = this.dialog.open(MessageExceptionsComponent, {
      width: '450px',
      data: {
        message: message.error
      }
    });
  }
}
