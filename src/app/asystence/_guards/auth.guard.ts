import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { LoginService } from 'src/app/pages/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  constructor(private router: Router,
              private serviceLogin: LoginService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const user = JSON.parse(localStorage.getItem('_user'));
    if (user) {
        // this.checkUser(user.email);
        return true;
        // return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  // checkUser(email): any {
  //   this.serviceAdminLogin.checkUser(email)
  //   .subscribe((res: any) => {
  //     console.log('CHECKUSER');
  //     return true;
  //   }, (err: any) => {
  //     this.router.navigate(['/admin/login']);
  //     this.serviceAdminLogin.showSnackBar('Upss!! ' + err.error.message );
  //     return false;
  //   });
  // }

}
