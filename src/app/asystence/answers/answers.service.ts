import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class AnswersService {

  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllAnswers(): any {
   // return this.http.get(this.url + 'respuestas');
   return [{ id_respuesta: 1, custionario: { id_cuestionario: 1, nombre: 'CC' },
   horario: { materia: {nombre: 'Virtuales', carrera: 'ITI'}, hora_inicio: '08:00', hora_fin: '09:00',
   profesor: { nombre: 'Manuel Chavez' }}}];
  }

  /**
   * GET :: Respuestas
   *
   */

  getAnswersById(id): any {
    return this.http.get(this.url + 'cuestionario/' + id + '/respuestas'); // ver para revisar
    // return this.http.get(this.url + 'preguntas/' + id + '/show'); // ver para contestar
    // return { id_respuesta: 1, custionario: { id_cuestionario: 1, nombre: 'CC' },
    //         preguntas: [
    //           { pregunta_id: 1, nombre: '¿Asistió el Profesor?', tipo: 'radio',
    //             opciones: [{ nombre: 'Si'}, {nombre: 'No'}], respuesta: 'Si'},
    //           { pregunta_id: 2, nombre: 'Comentarios', tipo: 'libre',
    //           opciones: [], respuesta: ''},
    //           { pregunta_id: 3, nombre: '¿Grupo en clase?', tipo: 'checkbox',
    //             opciones: [{ nombre: 'Clase'}, {nombre: 'Video'}, {nombre: 'Afuera'}, {nombre: 'Examen'}], 
    //             respuesta: ['Video', 'Afuera']},
    //         ]};
  }


  getAnswares(id): any {
    return this.http.get(this.url + 'horario/' + id + '/respuestas');
  }

  getScheduleByClassroom(id): any {
    return this.http.get(this.url + 'salon/' + id + '/horarios');
  }

  saveAnswers(answers, id): any {
    const body = {
      respuestas: answers
    };
    return this.http.post(this.url + 'horario/' + id + '/cuestionario', body);
  }

  updateAnswers(answers, id_contesatdo): any {
    const body = {
      respuestas: answers
    };
    return this.http.put(this.url + 'horario/' + id_contesatdo + '/cuestionario', body);
  }




  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    if (!message.error.isTrusted) {
      const dialogRef = this.dialog.open(MessageExceptionsComponent, {
        width: '450px',
        data: {
          message: message.error
        }
      });
    }
  }
}
