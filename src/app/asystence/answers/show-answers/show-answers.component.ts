import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AnswersService } from '../answers.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { isObject } from 'util';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-show-answers',
  templateUrl: './show-answers.component.html',
  styleUrls: ['./show-answers.component.scss']
})
export class ShowAnswersComponent implements OnInit {
  id_answers: any;
  angForm: FormGroup;
  radio: any;
  select: any;
  // objectAnswers = {
  //  // preguntas: [{}]
  //   // respuestas: [{}]
  // };
  objectAnswers = {};
  answers = '';
  arrayAnswers: any;
  constructor(private fb: FormBuilder,
              private _answersService: AnswersService,
              private router: ActivatedRoute) {
                const params: any = router.params;
                this.id_answers = params._value.id;
                this.getAnswersById();
               }

  ngOnInit() {
  }

  getAnswersById(): void {
    this._answersService.getAnswersById(this.id_answers)
      .subscribe((res: any) => {
        // this.toFormGroup(res);
        // let resp: any = this._answersService.getAnswersById(this.id_answers);
        this.objectAnswers = res;
      }, (err: any) => {
        this._answersService.showSanckBar('Upss ' + err.errors.message);
      });
    // this.objectConverter(this._answersService.getAnswersById(this.id_answers));


    // let resp: any = this._answersService.getAnswersById(this.id_answers);
    // console.log('Resp ' + JSON.stringify(resp.preguntas));
    // this.objectAnswers = resp.preguntas;
    // console.log('Json ' + JSON.stringify(this.objectAnswers));
  }

  answersView(): void {
    let data: any = this.objectAnswers;
    let arrayAnseres = [];
    data.forEach((element: any) => {
      arrayAnseres.push({'id_pregunta': element.id, 'respuesta': element.respuesta});
    });
    console.log('Repsuetas Values ' + JSON.stringify(arrayAnseres));
    this.arrayAnswers = arrayAnseres;
    this.saveAnswers();
  }

  saveAnswers(): void {
    this._answersService.updateAnswers(this.arrayAnswers, this.id_answers)
    .subscribe((res: any) => {
      // this.toFormGroup(res);
      // let resp: any = this._answersService.getAnswersById(this.id_answers);
      this._answersService.showSanckBar('Respuestas Guardadas');
      this.objectAnswers = res.preguntas;
    }, (err: any) => {
      this._answersService.showMessageExcep(err);
    });
  }

  toFormGroup(questions: [] ) {
    let group: any = {};

    questions.forEach((question: any) => {
      group[question.pregunta_id] = new FormControl('' || '');
    });
    return new FormGroup(group);
  }

  objectConverter(array: any): any {
    let object = [];
    array.forEach((element: any) => {
      console.log('Element ' + JSON.stringify(this.transform(element.preguntas)));
      object.push(element.preguntas);
    });
    this.objectAnswers = this.transform(object);

    // this.objectAnswers.preguntas = this.transform(this.objectConverter(object));
  }

  transform(value: any, args?: any[]): any[] {
    // check if 'routes' exists
    if (value) {
        // create instance vars to store keys and final output
        const keyArr: any[] = Object.keys(value),
            dataArr = [];
        // loop through the object,
        // pushing values to the return array
        keyArr.forEach((key: any) => {
            dataArr.push(value[key]);
        });
        // return the resulting array
        return dataArr;
    }
  }


  onSubmit() {
    // console.log('Respuestas ' + JSON.stringify(this.angForm.value) + '--- Radio: ' + this.radio + ' ----Select: ' + this.select);
    let data: any = Object.values(this.objectAnswers);
    console.log('Data Values ' + JSON.stringify(data));
    let arrayAnseres = [];
    let body: any;
    data.forEach((element: any) => {
      arrayAnseres.push({'id_question': element.preguntas.pregunta_id, 'respuesta': element.preguntas.respuesta});
    });
    console.log('Repsuetas Values ' + JSON.stringify(arrayAnseres));
  }





}
