import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';
import { MatDialog, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { AnswersService } from './answers.service';
import { AddEditSubjectsComponent } from '../subjects/add-edit-subjects/add-edit-subjects.component';
import { ShowAnswersComponent } from './show-answers/show-answers.component';
import { ClassroomService } from '../buildings/classroom-by-buildings/classroom.service';
import { BuildingsService } from '../buildings/buildings.service';

export interface StaffData {
  id: string;
  date: string;
  carrera: string;
}

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss']
})
export class AnswersComponent implements OnInit {
  /**
   * Compomete para gestionar las respuestas
   */

  displayedColumns: string[] = ['id', 'date', 'options'];
  dataSource: MatTableDataSource<StaffData>;

  classroomList: any;
  buildingsList: any;
  schedulesList: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              private _answersService: AnswersService,
              private _classroomService: ClassroomService,
              private _buildingsSerice: BuildingsService) {
                this.getBuildings();
  }

  ngOnInit() {
  }

  getClassroomByBuilding(id): void {
    this._classroomService.getClassroomsByBuilding(id)
      .subscribe((res: any) => {
        this.classroomList = res;
      }, (err: any) => {
        this._answersService.showMessageExcep(err);
    });
  }

  getBuildings(): void {
    this._buildingsSerice.getListBuildings()
      .subscribe((res: any) => {
        this.buildingsList = res;
      }, (err: any) => {
        this._answersService.showMessageExcep(err);
    });
  }

  getScheduleByClassroom(id): void {
    this._answersService.getScheduleByClassroom(id)
      .subscribe((res: any) => {
        this.schedulesList = res;
      }, (err: any) => {
        this._answersService.showMessageExcep(err);
    });
  }



  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAnswares(id): void {
    this._answersService.getAnswares(id)
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err: any) => {
        this._answersService.showSanckBar('Upss ' + err.errors.message);
      });
  }

  showAnswers(id): void {
    const dialogRef = this.dialog.open(ShowAnswersComponent, {
      width: '650px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.get();
    });
  }

}
