import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';
  
  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllSchedules(): any {
    return this.http.get(this.url + 'horarios');
  }

  getAllSchedulesBySchedule(): any {
    return this.http.get(this.url + 'horarios');
  }

  getScheduleById(id): any {
    return this.http.get(this.url + 'horarios/' + id + '/edit');
  }

  saveSchedule(formData): any {
    const body  = {
      semestre_id: formData.value.semester,
      salon_id: formData.value.classroom,
      materia_id: formData.value.subject,
      profesor_id: formData.value.teacher,
      cuestionario_id: formData.value.questionnarie,
      dias: formData.value.days,
      hora_inicio: formData.value.start,
      hora_fin: formData.value.end
    };
    return this.http.post(this.url + 'horarios/store', body);
  }

  editSchedule(id, formData): any {
    const body  = {
      semestre_id: formData.value.semester,
      salon_id: formData.value.classroom,
      materia_id: formData.value.subject,
      profesor_id: formData.value.teacher,
      cuestionario_id: formData.value.questionnarie,
      dias: formData.value.days,
      hora_inicio: formData.value.start,
      hora_fin: formData.value.end
    };
    return this.http.put(this.url + 'horarios/' + id + '/update', body);
  }

  deleteSchedule(id): any {
    return this.http.delete(this.url + 'horarios/' + id + '/destroy');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    if (!message.error.isTrusted) {
      const dialogRef = this.dialog.open(MessageExceptionsComponent, {
        width: '450px',
        data: {
          message: message.error
        }
      });
    }
  }
}
