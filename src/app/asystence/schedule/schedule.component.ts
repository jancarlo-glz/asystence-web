import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ScheduleService } from './schedule.service';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';
 
export interface TeacherData {
  semester: string;
  subject: string;
  teacher: string;
  classroom: string;
  days: string;
  times: string;
}

/** Datos de Prueba para ayudar a mostrar informacion dentro de la tabla */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  displayedColumns: string[] = ['semester', 'subject', 'teacher', 'classroom', 'days', 'times', 'options'];
  dataSource: MatTableDataSource<TeacherData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _scheduleService: ScheduleService,
              public dialog: MatDialog) {
    // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
    this.getAllSchedule();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.getAllStaff();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllSchedule(): void {
    this._scheduleService.getAllSchedules()
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
      });
  }


  deleteSchedule(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._scheduleService.deleteSchedule(id)
          .subscribe(arg => {
            this._scheduleService.showSanckBar('¡Datos Eliminados!');
            this.getAllSchedule();
          }, (err: any) => {
            this._scheduleService.showSanckBar('¡Upss! ' + err.errors.error.message);
        });
      }
    });
  }

}

/** Builds and returns a new User. */
function createNewUser(id: number): TeacherData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    semester: Math.round(Math.random() * 100).toString(),
    subject: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    teacher: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    classroom: Math.round(Math.random() * 100).toString(),
    days: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    times: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}
