import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ScheduleService } from '../schedule.service';
import { TeachersService } from '../../teachers/teachers.service';
import { ClassroomService } from '../../buildings/classroom-by-buildings/classroom.service';
import { QuestionnariesService } from '../../questionnaires/questionnaries.service';
import { BuildingsService } from '../../buildings/buildings.service';
import { SubjectsService } from '../../subjects/subjects.service';
import { SemesterService } from '../../semester/semester.service';

@Component({
  selector: 'app-add-edit-schedule',
  templateUrl: './add-edit-schedule.component.html',
  styleUrls: ['./add-edit-schedule.component.scss']
})
export class AddEditScheduleComponent implements OnInit {
  angForm: FormGroup;
  type: any;
  id_schedule: any;
  building: any;
  teacherList = [];
  classroomList = [];
  questionnarieList = [];
  buildingsList = [];
  subjectsList = [];
  daysList = [
    {value: 'lunes', name: 'Lunes'},
    {value: 'martes', name: 'Martes'},
    {value: 'miercoles', name: 'Miercoles'},
    {value: 'jueves', name: 'Jueves'},
    {value: 'viernes', name: 'Viernes'},
    {value: 'sabado', name: 'Sabado'},
    {value: 'domingo', name: 'Domingo'},
  ];
  semestersList = [
    {id: '1', nombre: '1 Semestre'},
    {id: '2', nombre: '2 Semestre'},
    {id: '3', nombre: '3 Semestre'},
    {id: '4', nombre: '4 Semestre'},
    {id: '5', nombre: '5 Semestre'},
    {id: '6', nombre: '6 Semestre'},
  ];
  careerList = [
    {id: 'ITI', nombre: 'Tecnologías I'},
    {id: 'ITMA', nombre: 'Manufactura'},
    {id: 'ITEM', nombre: 'Telematica'},
    {id: 'ISTI', nombre: 'Sistemas'},
  ];
  hoursList = [
    { value: '01:00', time: '01:00'},
    { value: '02:00', time: '02:00'},
    { value: '03:00', time: '03:00'},
    { value: '04:00', time: '04:00'},
    { value: '05:00', time: '05:00'},
    { value: '6:00', time: '6:00'},
    { value: '7:00', time: '7:00'},
    { value: '8:00', time: '8:00'},
    { value: '9:00', time: '9:00'},
    { value: '10:00', time: '10:00'},
    { value: '11:00', time: '11:00'},
    { value: '12:00', time: '12:00'},
    { value: '13:00', time: '13:00'},
    { value: '14:00', time: '14:00'},
    { value: '15:00', time: '15:00'},
    { value: '16:00', time: '16:00'},
    { value: '17:00', time: '17:00'},
    { value: '18:00', time: '18:00'},
    { value: '19:00', time: '19:00'},
    { value: '20:00', time: '20:00'},
    { value: '21:00', time: '21:00'},
    { value: '22:00', time: '22:00'},
    { value: '23:00', time: '23:00'},
    { value: '00:00', time: '00:00'},
  ];
  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private _scheduleService: ScheduleService,
    private _teacherService: TeachersService,
    private _classroomService: ClassroomService,
    private _questionnariesService: QuestionnariesService,
    private _buildingsSerice: BuildingsService,
    private _subjectsService: SubjectsService,
    private _semesterService: SemesterService,
    private router: Router) {
      const params: any = this.route.params;
      this.type = params._value.type;
      if (this.type === 'edit') {
        this.id_schedule = params._value.id;
        this.getInfoSchedule();
      }
      this.createForm();
      this.getCareers();
      this.getSemesters();
      this.getTeachers();
      this.getClassroom();
      this.getSubjects();
      this.getQuestionnaries();
      this.getBuildings();
      // this.getSemesters();
     }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      semester: ['', Validators.required ],
      classroom: ['', Validators.required],
      subject: ['', Validators.required ],
      teacher: ['', Validators.required ],
      questionnarie: ['', Validators.required ],
      days: ['', Validators.required ],
      start: ['', Validators.required ],
      end: ['', Validators.required ]
    });
  }

  getCareers(): void {
    this._teacherService.getCareers()
      .subscribe((res: any) => {
        this.careerList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getTeachers(): void {
    this._teacherService.getAllTeachers()
      .subscribe((res: any) => {
        this.teacherList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getClassroom(): void {
    this._classroomService.getAllClassroom()
      .subscribe((res: any) => {
        this.classroomList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getClassroomByBuilding(id): void {
    this._classroomService.getClassroomsByBuilding(id)
      .subscribe((res: any) => {
        this.classroomList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getBuildings(): void {
    this._buildingsSerice.getListBuildings()
      .subscribe((res: any) => {
        this.buildingsList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getSubjects(): void {
    this._subjectsService.getAllSubjects()
      .subscribe((res: any) => {
        this.subjectsList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getSemesters(): void {
    this._semesterService.getAllSemesters()
      .subscribe((res: any) => {
        this.semestersList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getQuestionnaries(): void {
    this._questionnariesService.getAllQuestionnaries()
      .subscribe((res: any) => {
        this.questionnarieList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  getInfoTeacher(): void {
    this._teacherService.getSubjectsOfTeacher(this.angForm.value.teacher)
      .subscribe((res: any) => {
        this.subjectsList = res;
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }


  // searchSubjects(): void {
  //   if (this.career && this.angForm.value.semester) {
  //     this._teacherService.getSubjectsByCareerAndSemester(this.career, this.angForm.value.semester)
  //       .subscribe((res: any) => {
  //         this.careerList = res;
  //       }, (err: any) => {
  //         this._teacherService.showSanckBar('Upss! ' + err.errors);
  //     });
  //   } else {
  //     this._scheduleService.showSanckBar('Seleccionar el Semestere y Carrea');
  //   }
  // }

  getInfoSchedule(): void {
    this._scheduleService.getScheduleById(this.id_schedule)
      .subscribe((res: any) => {
        this.angForm.controls.semester.setValue(res.semestre_id);
        this.angForm.controls.classroom.setValue(res.salon_id);
        this.angForm.controls.subject.setValue(res.materia_id);
        this.angForm.controls.teacher.setValue(res.profesor_id);
        this.angForm.controls.questionnarie.setValue(res.cuestionario_id);
        this.getInfoTeacher();
        this.angForm.controls.days.setValue(res.dias);
        this.angForm.controls.start.setValue(res.horario.hora_inicio);
        this.angForm.controls.end.setValue(res.horario.hora_fin);
        console.log('Horario ' + JSON.stringify(res.horario));
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
    });
  }

  addSchedule(): void {
    this._scheduleService.saveSchedule(this.angForm)
      .subscribe((res: any) => {
        this._scheduleService.showSanckBar('¡Datos Guardados! ');
        this.router.navigate(['/schedule']);
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
      });
  }

  editSchedule(): void {
    this._scheduleService.editSchedule(this.id_schedule, this.angForm)
      .subscribe((res: any) => {
        this._scheduleService.showSanckBar('¡Datos Guardados! ');
        this.router.navigate(['/schedule']);
      }, (err: any) => {
        this._scheduleService.showMessageExcep(err);
      });
  }

}
