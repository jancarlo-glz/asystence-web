import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-confirm-deleted',
  templateUrl: './confirm-deleted.component.html',
  styleUrls: ['./confirm-deleted.component.scss']
})
export class ConfirmDeletedComponent implements OnInit {
  nameDelete: any;
  flagClose: boolean;

  constructor( public dialogRef: MatDialogRef<ConfirmDeletedComponent>,
    @Inject(MAT_DIALOG_DATA) public _data) {
      this.nameDelete = _data.id;
      this.flagClose = true;
    }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
