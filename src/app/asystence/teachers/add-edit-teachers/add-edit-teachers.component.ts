import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TeachersService } from '../teachers.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ShowSubjectsComponent } from '../../subjects/show-subjects/show-subjects.component';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-add-edit-teachers',
  templateUrl: './add-edit-teachers.component.html',
  styleUrls: ['./add-edit-teachers.component.scss']
})
export class AddEditTeachersComponent implements OnInit {

  angForm: FormGroup;
  type: any;
  id_teacher: any;
  fileToUpload: any;
  subjectsTeacherShow = [];
  subjectsTeacherHandler = [];
  career: any;
  search: boolean;
  enabled_subjects = false;
  semester: any;
  selectable = false;
  removable = true;
  name: any;
  countSubjects: any;
  semesterList = [
    {id: '1', nombre: '1 Semestre'},
    {id: '2', nombre: '2 Semestre'},
    {id: '3', nombre: '3 Semestre'},
    {id: '4', nombre: '4 Semestre'},
    {id: '5', nombre: '5 Semestre'},
    {id: '6', nombre: '6 Semestre'},
  ];
  careerList = [
    {id: 'ITI', nombre: 'Tecnologías I'},
    {id: 'ITMA', nombre: 'Manufactura'},
    {id: 'ITEM', nombre: 'Telematica'},
    {id: 'ISTI', nombre: 'Sistemas'},
  ];
  image: any;
  url: any;

  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private _teacherService: TeachersService) {
                const params: any = this.route.params;
                this.type = params._value.type;
                if (this.type === 'edit') {
                  this.id_teacher = params._value.id;
                  this.getInfoTeacher();
                  this.enabled_subjects = true;
                }
                this.createForm();
                this.getCareers();
                this.url = this._teacherService.url_base;
                // this.getSemesters();
               }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      lastname: ['', Validators.required],
      beginning: ['', Validators.required ],
      birth: ['', Validators.required ],
      email: ['', Validators.required ]
    });
  }

  onFileChangedImport(files: FileList): any {
    this.fileToUpload = files.item(0);
  }

  getInfoTeacher(): void {
    this._teacherService.getTeacherById(this.id_teacher)
      .subscribe((res: any) => {
        this.name = res.nombre +  ' ' + res.apellido;
        this.angForm.controls.name.setValue(res.nombre);
        this.angForm.controls.lastname.setValue(res.apellido);
        this.angForm.controls.beginning.setValue(res.fecha_ingreso);
        this.angForm.controls.birth.setValue(res.fecha_nacimiento);
        this.angForm.controls.email.setValue(res.email);
        this.image = res.path_foto;
        this.getSubjectsTeacher();
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  getSubjectsTeacher(): void {
    this._teacherService.getSubjectsOfTeacher(this.id_teacher)
      .subscribe((res: any) => {
        if (res !== [] && res !== 'Error') {
          this.subjectsTeacherHandler = Object.keys(res);
          this.subjectsTeacherShow =  Object.values(res);
          console.log(' Show ' + JSON.stringify(this.subjectsTeacherShow));
          console.log(' Handler ' + JSON.stringify(this.subjectsTeacherHandler));
        }
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  getCareers(): void {
    this._teacherService.getCareers()
      .subscribe((res: any) => {
        this.careerList = res;
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  getSemesters(): void {
    this._teacherService.saveTeacher(this.angForm, this.fileToUpload)
      .subscribe((res: any) => {
        this.semesterList = res;
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  changeSearch(): void {
    this.search = !this.search;
  }

  addTeacher(): void {
    this._teacherService.saveTeacher(this.angForm, this.fileToUpload)
      .subscribe((res: any) => {
        this.enabled_subjects = true;
        this.router.navigate(['/teacher']);
        this._teacherService.showSanckBar('¡Datos Guardados! ');
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
  }

  editTeacher(): void {
    if (!this.fileToUpload) {
    this._teacherService.editTeacher(this.id_teacher, this.angForm, this.fileToUpload)
      .subscribe((res: any) => {
        this._teacherService.showSanckBar('¡Datos Guardados! ');
        this.getInfoTeacher();
      }, (err: any) => {
        this._teacherService.showMessageExcep(err);
      });
    }
    if (this.fileToUpload) {
      this._teacherService.editTeacher(this.id_teacher, this.angForm, null)
        .subscribe((res: any) => {
          this._teacherService.showSanckBar('¡Datos Guardados! ');
        }, (err: any) => {
          this._teacherService.showMessageExcep(err);
      });
      this._teacherService.updatePhoto(this.id_teacher, this.fileToUpload)
        .subscribe((res: any) => {
          this._teacherService.showSanckBar('Foto Actualizada! ');
          this.getInfoTeacher();
        }, (err: any) => {
          this._teacherService.showMessageExcep(err);
      });
      }
  }

  showSubjects(): void {
    const dialogRef = this.dialog.open(ShowSubjectsComponent, {
      width: '650px',
      data: {
        semester: this.semester,
        career: this.career,
        id_teacher: this.id_teacher
      }
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        console.log(' Show ' + JSON.stringify(this.subjectsTeacherShow));
        console.log(' Handler ' + JSON.stringify(this.subjectsTeacherHandler));
        console.log('Array ' + JSON.stringify(result));
        let arrayTemp = [];
        let arrayKeys = [];
        if (this.subjectsTeacherShow && this.subjectsTeacherHandler) {
          this.subjectsTeacherShow.forEach(element =>{
            arrayTemp.push(element);
          });
          // arrayTemp.push(this.subjectsTeacherShow);
          // arrayKeys.push(this.subjectsTeacherHandler);
          this.subjectsTeacherHandler.forEach(element => {
            arrayKeys.push(element);
          });
        }
        result.forEach((element) => {
           console.log('key ' + element.key);
           // this.subjectsTeacher = Object.assign(this.subjectsTeacher, element.key);
           arrayTemp.push(element.value);
           arrayKeys.push(element.key);
        });
        this.subjectsTeacherShow = arrayTemp;
        this.subjectsTeacherHandler = arrayKeys;
        console.log(' Show ' + JSON.stringify(this.subjectsTeacherShow));
        console.log(' Handler ' + JSON.stringify(this.subjectsTeacherHandler));
      }
    });
  }

  saveSubjects(): void {
    this._teacherService.saveSubjects(this.id_teacher, this.subjectsTeacherHandler)
    .subscribe((res: any) => {
      this._teacherService.showSanckBar('Materias Guardadas! ');
      this.getSubjectsTeacher();
    }, (err: any) => {
      this._teacherService.showMessageExcep(err);
    });
  }

  removeChip(item): void {
   const index = this.subjectsTeacherShow.indexOf(item);
    if (index >= 0) {
        this.subjectsTeacherShow.splice(index, 1);
        this.subjectsTeacherHandler.splice(index, 1);
    }
  }

}
