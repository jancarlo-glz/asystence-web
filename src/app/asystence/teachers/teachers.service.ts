import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  url = 'http://18.222.150.29/api/';
  url_base = 'http://18.222.150.29';
  // url = 'http://192.168.10.129/api/';
  // url_base = 'http://192.168.10.129';


  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllTeachers(): any {
    return this.http.get(this.url + 'profesores');
  }

  getTeacherById(id): any {
    return this.http.get(this.url + 'profesores/' + id + '/edit');
  }

  getCareers(): any {
    return this.http.get(this.url + 'getCarreras');
  }

  getSubjectsByCareer(career): any {
    return this.http.get(this.url + 'getMaterias/' + career);
  }

  getSubjectsByCareerAndSemester(career, semester): any {
    return this.http.get(this.url + 'getMaterias/' + career + '/' + semester);
  }

  getSubjectsByCareerAndSemesterWithOutHas(career, semester, teacher): any {
    return this.http.get(this.url + 'getMaterias/' + career + '/' + semester + '/' + teacher);
  }

  getSubjectsOfTeacher(id): any {
    return this.http.get(this.url + 'profesores/' + id + '/getMaterias' );
  }

  saveTeacher(formData, file): any {
    if (file) {
      const body = new FormData();
      body.append('nombre', formData.value.name);
      body.append('apellido', formData.value.lastname);
      body.append('fecha_ingreso', formData.value.beginning);
      body.append('fecha_nacimiento', formData.value.birth);
      body.append('email', formData.value.email);
      body.append('image', file, file.name);
    return this.http.post(this.url + 'profesores/store', body);
    }
    if (!file) {
      const body  = {
        nombre: formData.value.name,
        apellido: formData.value.lastname,
        fecha_ingreso: formData.value.beginning,
        fecha_nacimiento: formData.value.birth,
        email: formData.value.email,
      };
      return this.http.post(this.url + 'profesores/store', body);
    }
  }

  editTeacher(id, formData, file): any {
    if (!file) {
      const body  = {
        nombre: formData.value.name,
        apellido: formData.value.lastname,
        fecha_ingreso: formData.value.beginning,
        fecha_nacimiento: formData.value.birth,
        email: formData.value.email,
      };
      return this.http.put(this.url + 'profesores/' + id + '/update', body);
    } else {
      const body = new FormData();
      body.append('nombre', formData.value.name);
      body.append('apellido', formData.value.lastname);
      body.append('feha_ingreso', formData.value.beginning);
      body.append('feha_nacimiento', formData.value.birth);
      body.append('email', formData.value.email);
      body.append('image', file, file.name);
      return this.http.put(this.url + 'profesores/' + id + '/update', body);
    }
  }

  updatePhoto(id, file): any {
    const body = new FormData();
      body.append('image', file, file.name);
      return this.http.post(this.url + 'profesores/' + id + '/updateAvatar', body);
  }

  saveSubjects(id, subjects): any {
    const body = {
      materia_id: subjects
    };
    return this.http.put(this.url + 'profesores/' + id + '/storeMateria', body);
  }

  deleteTeacher(id): any {
    return this.http.delete(this.url + 'profesores/' + id + '/destroy');
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    if (!message.error.isTrusted) {
      const dialogRef = this.dialog.open(MessageExceptionsComponent, {
        width: '450px',
        data: {
          message: message.error
        }
      });
    }
  }
}
