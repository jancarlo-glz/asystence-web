import { Component, OnInit, ViewChild } from '@angular/core';
import { TeachersService } from './teachers.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';

export interface TeacherData {
  nombre: string;
  fecha_nacimiento: string;
  fecha_inicio: string;
  email: string;
}

/** Datos de Prueba para ayudar a mostrar informacion dentro de la tabla */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];


@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  displayedColumns: string[] = ['name', 'birth', 'beginning', 'email', 'options'];
  dataSource: MatTableDataSource<TeacherData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _teachersService: TeachersService,
              public dialog: MatDialog) {
    // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
    this.getAllTeachers();
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // this.getAllStaff();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllTeachers(): void {
    this._teachersService.getAllTeachers()
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err: any) => {
        this._teachersService.showMessageExcep(err);
      });
  }


  deleteTeacher(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._teachersService.deleteTeacher(id)
          .subscribe(arg => {
            this._teachersService.showSanckBar('¡Datos Eliminados!');
            this.getAllTeachers();
          }, (err: any) => {
            this._teachersService.showMessageExcep(err);
          });
      }
    });
  }

}

/** Builds and returns a new User. */
function createNewUser(id: number): TeacherData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    fecha_inicio: Math.round(Math.random() * 100).toString(),
    email: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    nombre: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    fecha_nacimiento: Math.round(Math.random() * 100).toString(),
  };
}
