import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

// Service utilizado para realizar las llamas a la API REST mediante HTTP
@Injectable({
  providedIn: 'root'
})
export class BuildingsService {
  // GET /getEdificios
  // GET /getCarreras
  // GET /getMaterias/:id_carrera  Materias por carrera
  // GET /getMaterias/:id_carrera/:id_semestre(numero) Materias por carrera por semestre

  url = 'http://18.222.150.29/api/';  // url del servidor
  url_base = 'http://18.222.150.29';
  // url = 'http://192.168.10.129/api/';
  // url_base = 'http://192.168.10.129';

  constructor(private http: HttpClient, // Objeto de HttpClient para acceder a los metodos
              private snackBar: MatSnackBar, // Obejto de la clase MatSnackBar para mostart alertas en Toast
              private dialog: MatDialog) { }


  // Ruta de la API que retorna todos los edificios en lista
  getListBuildings(): any {
    return this.http.get(this.url + 'getEdificios');
  }

  // Ruta de la API que retorna todos los Edificios en Arreglo
  getAllBuildings(): any {
    return this.http.get(this.url + 'edificios');
  }

  // Ruta de la API que retorna la infromación de un Edificio especifico
  getBuildingById(id): any {
    return this.http.get(this.url + 'edificios/' + id + '/edit',  { headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }});
  }

  // Ruta de la API que crea un registro nuevo de un Edificio
  addBuilding(formData): any {
    const body = {
      edificio_id: formData.value.name,
      capacidad: formData.value.capacity,
      caracteristica: formData.value.features
    };
    return this.http.post(this.url + 'edificios/store', body, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }});
  }

  // Ruta de la API que actualiza un registro de un Edificio
  editBuilding(id, formData): any {
    const body = {
      edificio_id: formData.value.name,
      capacidad: formData.value.capacity,
      caracteristica: formData.value.features
    };
    return this.http.put(this.url + 'edificios/' + id + '/update', body);
  }

  // Ruta de la API que elimina un registro de un Edificio
  deleteBuilding(id): any {
    return this.http.delete(this.url + 'edificios/' + id + '/destroy');
  }

  // Ruta de la API que retorna los sales por edificio
  getClassroomsByBuilding(id): any {
    return this.http.get(this.url + 'building/classroom/' + id);
  }

  // Metodo que muestra los mensajes en Toast
  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  // Metodo que llama a un Dialogo para mostrar los mensajes de error o validaciones del servidor
  showMessageExcep(message): void {
    if (!message.error.isTrusted) {
      const dialogRef = this.dialog.open(MessageExceptionsComponent, {
        width: '450px',
        data: {
          message: message.error
        }
      });
    }
  }
}
