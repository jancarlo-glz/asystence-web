import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {
  url = 'http://18.222.150.29/api/';
  url_base = 'http://18.222.150.29';
  // url = 'http://192.168.10.129/api/';
  // url_base = 'http://192.168.10.129';
  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

  getAllClassroom(): any {
    return this.http.get(this.url + 'salones');
  }

  getClassroomById(id): any {
    return this.http.get(this.url + 'salones/' + id + '/edit');
  }

  addClassroom(formData): any {
    const body = {
      salon_id: formData.value.name,
      edificio_id: formData.value.building,
      caracteristica: formData.value.features,
      tipo: formData.value.type,
      capacidad: formData.value.capacity
    };
    return this.http.post(this.url + 'salones/store', body);
  }

  editClassroom(id, formData): any {
    const body = {
      salon_id: formData.value.name,
      edificio_id: formData.value.building,
      caracteristica: formData.value.features,
      tipo: formData.value.type,
      capacidad: formData.value.capacity
    };
    return this.http.put(this.url + 'salones/' + id + '/update', body);
  }

  deleteClassroom(id): any {
    return this.http.delete(this.url + 'salones/' + id + '/destroy');
  }

  getClassroomsByBuilding(id): any {
    return this.http.get(this.url + 'salones/' + id);
  }

  showSanckBar(message): void {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  showMessageExcep(message): void {
    if (!message.error.isTrusted) {
      const dialogRef = this.dialog.open(MessageExceptionsComponent, {
        width: '450px',
        data: {
          message: message.error
        }
      });
    }
  }
}
