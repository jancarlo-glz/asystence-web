import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditClassroomComponent } from './add-edit-classroom.component';

describe('AddEditClassroomComponent', () => {
  let component: AddEditClassroomComponent;
  let fixture: ComponentFixture<AddEditClassroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditClassroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditClassroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
