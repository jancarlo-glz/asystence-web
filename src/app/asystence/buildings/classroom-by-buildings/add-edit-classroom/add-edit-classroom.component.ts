import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClassroomService } from '../classroom.service';
import { BuildingsService } from '../../buildings.service';

@Component({
  selector: 'app-add-edit-classroom',
  templateUrl: './add-edit-classroom.component.html',
  styleUrls: ['./add-edit-classroom.component.scss']
})
export class AddEditClassroomComponent implements OnInit {
  type: any;
  id_building;
  id_classroom: any;
  angForm: FormGroup;
  buildingList: any = [
    { edificio_id: 'UA1' },
    { edificio_id: 'UA2' },
    { edificio_id: 'UA3' },
    { edificio_id: 'UA4' },
    { edificio_id: 'CC' },
  ];
  typeList: any = [
    { value: 'Alumnos', name: 'Alumnos' },
    { value: 'Cubiculos', name: 'Cubículos' },
    { value: 'Laboratorios', name: 'Laboratorios' },
    { value: 'Computo', name: 'Cómputo' },
    { value: 'Audio-visual', name: 'Audiovisual' }
  ];

  constructor(public dialogRef: MatDialogRef<AddEditClassroomComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _classroomService: ClassroomService,
              private _buldingService: BuildingsService) {
      this.createForm();
      this.type = _data.type;
      if (this.type === 'add') {
        this.angForm.controls.building.setValue(_data.id);
        console.log('ID Add ' + this.angForm.value.building);
      }
      console.log('Type ' + _data.type );
      if (this.type === 'edit') {
        this.id_classroom = _data.id;
        this.getClassroomById();
      }
      this.getListBuildings();
     }

  ngOnInit() {
   // this.getAllBuildings();
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      capacity: ['', Validators.required],
      building: ['', Validators.required],
      features: ['', Validators.required ],
      type: ['', Validators.required ],
    });
  }

  getListBuildings(): void {
    this._buldingService.getListBuildings()
      .subscribe((res: any) => {
        this.buildingList = res;
      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }

  getAllBuildings(): void {
    this._buldingService.getAllBuildings()
      .subscribe((res: any) => {
        this.buildingList = res;
      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }

  getClassroomById(): void {
    this._classroomService.getClassroomById(this.id_classroom)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.salon.salon_id);
        this.angForm.controls.capacity.setValue(res.salon.capacidad);
        this.angForm.controls.features.setValue(res.salon.caracteristica);
        this.angForm.controls.building.setValue(res.salon.edificio_id);
        this.angForm.controls.type.setValue(res.salon.tipo);

      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }

  addClassroom(): void {
    console.log('Name ' + this.angForm.value.name);
    this._classroomService.addClassroom(this.angForm)
      .subscribe((res: any) => {
        this._classroomService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }

  editClassroom(): void {
    this._classroomService.editClassroom(this.id_classroom, this.angForm)
      .subscribe((res: any) => {
        this.dialogRef.close();
        this._classroomService.showSanckBar('¡Datos Agregados!');
      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }


}
