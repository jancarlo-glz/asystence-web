import { Component, OnInit } from '@angular/core';
import { BuildingsService } from '../buildings.service';
import { ActivatedRoute } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import { AddEditClassroomComponent } from './add-edit-classroom/add-edit-classroom.component';
import { ConfirmDeletedComponent } from '../../confirm-deleted/confirm-deleted.component';
import { ClassroomService } from './classroom.service';

@Component({
  selector: 'app-classroom-by-buildings',
  templateUrl: './classroom-by-buildings.component.html',
  styleUrls: ['./classroom-by-buildings.component.scss']
})
export class ClassroomByBuildingsComponent implements OnInit {

  building_name: any;
  classroomsList: any = [
    // { salon_id: 'A1', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '45 Lugares', capacidad: 30 },
    // { salon_id: 'A2', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '40 Lugares', capacidad: 39 },
    // { salon_id: 'A3', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '40 Lugares', capacidad: 40 },
    // { salon_id: 'A4', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '38 Lugares', capacidad: 40 },
    // { salon_id: 'A5', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '40 Lugares', capacidad: 39 },
    // { salon_id: 'A6', edificio_id: 'UA1', tipo: 'Clases', caracteristica: '45 Lugares', capacidad: 30 },
  ];
  url: any;
  constructor(private _buildingsService: BuildingsService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private _classroomService: ClassroomService) {
                const id: any = this.route.params;
                this.building_name = id._value.id;
                this.getClassroomByBuilding();
                this.url = this._buildingsService.url_base;
              }

  ngOnInit() {

  }

  getClassroomByBuilding(): void {
    this._classroomService.getClassroomsByBuilding(this.building_name)
      .subscribe((res: any) => {
        this.classroomsList = res;
      }, (err: any) => {
        this._classroomService.showMessageExcep(err);
      });
  }

  downloadQr(qr): any {
    window.open(this.url + qr, '_blank');
  }

  addClassroom(): void {
    const dialogRef = this.dialog.open(AddEditClassroomComponent, {
      width: '650px',
      data: {
        type: 'add',
        id: this.building_name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getClassroomByBuilding();
    });
  }

  editClassroom(id): void {
    const dialogRef = this.dialog.open(AddEditClassroomComponent, {
      width: '650px',
      data: {
        type: 'edit',
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getClassroomByBuilding();
    });
  }

  deleteClassroom(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '350px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._classroomService.deleteClassroom(id)
          .subscribe(arg => {
            this.getClassroomByBuilding();
            this._classroomService.showSanckBar('¡Datos Eliminados!');
          }, (err: any) => {
            this._classroomService.showMessageExcep(err);
          });
      }
    });
  }

}
