import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BuildingsService } from '../buildings.service';

@Component({
  selector: 'app-add-edit-buildings',
  templateUrl: './add-edit-buildings.component.html',
  styleUrls: ['./add-edit-buildings.component.scss']
})


export class AddEditBuildingsComponent implements OnInit {
  type: any;
  id_building: any;
  angForm: FormGroup; // Declaramos variable de tipo Grupo de Formulario
  constructor(public dialogRef: MatDialogRef<AddEditBuildingsComponent>, // Indicamos que la clase se va a comportar como un Dialogo
              private fb: FormBuilder, // Objeto de la clase FormBuilder
              @Inject(MAT_DIALOG_DATA) public _data, // Atravez del Inject obtenemos los datos que nos pasaron de la clase.
              private _buildingsService: BuildingsService) {
      // Validamos el tipo para saber si se va a crear o editar un registro
      this.type = _data.type;
      // En caso de recibir id obtener los datos para editar
      if (_data.id) {
        this.id_building = _data.id;
        this.getBuildingById();
      }
      // Creamos el formulario
      this.createForm();
     }

  ngOnInit() {
  }
  // Metodo de Creación de Formulario, con campos necesarios.
  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      capacity: ['', Validators.required],
      features: ['', Validators.required ],
    });
  }
  // Metodo que tare información del edificio
  getBuildingById(): void {
    this._buildingsService.getBuildingById(this.id_building)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.edificio_id);
        this.angForm.controls.capacity.setValue(res.capacidad);
        this.angForm.controls.features.setValue(res.caracteristica);
      }, (err: any) => {
        this._buildingsService.showMessageExcep(err);
      });
  }

  // Metodo que guarda o crea el registro
  addBuilding(): void {
    this._buildingsService.addBuilding(this.angForm)
      .subscribe((res: any) => {
        this._buildingsService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._buildingsService.showMessageExcep(err);
      });
  }

  // Metodo que actualiza el registro actual
  editBuilding(): void {
    this._buildingsService.editBuilding(this.id_building, this.angForm)
      .subscribe((res: any) => {
        this._buildingsService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._buildingsService.showMessageExcep(err);
      });
  }


}
