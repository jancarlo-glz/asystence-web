// Importación de clases, modulos, directivas, providers que se utilizaran en la
// Clase. Esto se realiza en todos los componentes
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { AddEditBuildingsComponent } from './add-edit-buildings/add-edit-buildings.component';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';
import { BuildingsService } from './buildings.service';

// Interfaz Para modelar los datos a mostar en la Tabla según
// el tipado que tengan.
export interface UserData {
  id: string;
  features: string;
  capacity: string;
}

// Incialización del Componenete, indicando nombre del selector,
// dirección de la vista y los estilos.
@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.scss']
})

// Declaración  de la Clase
export class BuildingsComponent implements OnInit {

  // Varibles Globales.
  displayedColumns: string[] = ['id', 'features', 'capacity', 'options']; // Arreglo que nos pintaría las columnas a utilizar
  dataSource: MatTableDataSource<UserData>; // Variable que contendra los datos de la tabla
                                            // de tipo MatTableDataSource que dependerá del modelo User Data

  // Variables tomadas de la vista para usarlas como variables globales.
  @ViewChild(MatPaginator) paginator: MatPaginator; // variable para la paginación de la tabla.
  @ViewChild(MatSort) sort: MatSort; // variable para ordenar las columnas

  // delcaración de objetos (inatancias) hacia las clases a utilizar
  constructor(public dialog: MatDialog,
              private _buildingService: BuildingsService) {
    this.getAllBuildings();
  }
 // metedo de ejecucion inicial
  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // this.getAllBuildings();
  }
   // Metodo para filtar los datos en la tabla
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
 // Metodo que consulta la API mediante el service BuldingService
  getAllBuildings(): void {
    this._buildingService.getAllBuildings() // Llamada al methodo del service para realizar la petición HTTP.
      .subscribe((res: any) => { // Atarpar la respuesta de la petición.
        this.dataSource = new MatTableDataSource(res); // Asiganación de la información que no llega de la Petición Http.
        this.dataSource.paginator = this.paginator; // Inicialización de la paginación para usarse.
        this.dataSource.sort = this.sort; // Inicialización del Ordenamiento para usarse.
      }, (err: any) => {// Atrapar la respuesta erronea o fallida de la petición
        this._buildingService.showMessageExcep(err); // Mostrar Mesage de error en pantalla, llamando al metodo del service
      });
  }

  // Metodo que abre un Dialogo (Modal) para Agregar Edificios.
  addBuilding(): void {
    // Abrimos el Dialogo con la clase a mostrar.
    const dialogRef = this.dialog.open(AddEditBuildingsComponent, {
      width: '650px', // proiedad del tamaño.
      data: { // json a mandar con información necesaria para el dialogo.
        type: 'add' // Indicamos que se va a agregar.
      }
    });

    // Metodo que se ejecuta al cerrar el dialogo
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBuildings(); // Se llama al metodo para refrescar los datos de la tabla.
    });
  }

// Metodo que abre un Dialogo (Modal) para Agregar Edificios.
  editBuilding(id): void {
    const dialogRef = this.dialog.open(AddEditBuildingsComponent, {
      width: '650px',
      data: {
        type: 'edit', // Indicamos que se va a editar.
        id: id // se manda el id del registro para consultar
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllBuildings();
    });
  }

  // Metodo de Eliminar registro
  deleteBuilding(id): void {
    // Abrimos el Dialogo con la Clase Borrado que nos mostrara un confirmación
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id // enviamos el id del registro a eliminar para mostrarlo
      }
    });

    // Una ves que se cierre el dialogo verificamos si el boton de aceptar fue oprimido
    dialogRef.afterClosed().subscribe(result => {
      if (result) { // si acepto borrar el registro
        this._buildingService.deleteBuilding(id) // llamamos al metodo de la peticiòn HTTP del service, mandado el id como parametro
          .subscribe(arg => {  // atrapamos la respuesta correcta
            this._buildingService.showSanckBar('¡Datos Eliminados!'); // Mostramos mensaje de datos Borrados
            this.getAllBuildings(); // Rfrescamos la tabla
          }, (err: any) => { // En caso de error
            this._buildingService.showMessageExcep(err); // Mostrar Mesage de error en pantalla, llamando al metodo del service
          });
      }
    });
  }
}
