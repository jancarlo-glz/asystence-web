import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsystenceComponent } from './asystence.component';

describe('AsystenceComponent', () => {
  let component: AsystenceComponent;
  let fixture: ComponentFixture<AsystenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsystenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsystenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
