import { Component, OnInit, Inject} from '@angular/core';
import { StaffService } from '../staff.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-set-subjects-teacher',
  templateUrl: './set-subjects-teacher.component.html',
  styleUrls: ['./set-subjects-teacher.component.scss']
})
export class SetSubjectsTeacherComponent implements OnInit {
  id_staff: any;
  buildingsSelected = [];
  buildingsHave = [];
  buildingsList = [];
  constructor(public dialogRef: MatDialogRef<SetSubjectsTeacherComponent>,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _staffService: StaffService) {
                this.id_staff = _data.id;

              }

  ngOnInit() {
  }

  // setRouterPermisions(id: any): void {
  //   this._staffService.setBuildingsPermisions(this.id_user, id)
  //   .subscribe((res: any) => {
  //       const snackBarRef = this.snackBar.open('Datos actualizados', '✅', {
  //           duration: 1000
  //       });
  //   }, (err) => {
  //       const dataerr = err;
  //       this.authenticationService.showSnackBarUpss(dataerr, 'top');
  //   });
  // }

  // getEmployee(): any {
  //   let data: any;
  //   this.loading = true;
  //   this.authenticationService.getEmployee(this.id_user)
  //       .subscribe(res => {
  //           data = res;
  //           this.dataSelected =  this.authenticationService.transform(data.routers);
  //           this.loading = false;
  //           this.getRouters();
  //       }, (err) => {
  //           const dataerr = err;
  //           this.loading = false;
  //           this.authenticationService.showSnackBarUpss(dataerr, 'top');
  //       });
  // } 

  getStaffBuildings(): void { 
    this._staffService.getStaffById(this.id_staff)
    .subscribe((res: any) => {
        this.buildingsHave = res;
        if (this.buildingsHave.length > 0) {
            this.buildingsList = this.createRouterList(this.buildingsHave, this.buildingsSelected);
        } else {
          this.buildingsList = this.buildingsHave;
        }
    }, (err) => {
        const dataerr = err;
    });
  }

  createRouterList(arrayBase: any, arrayQuit: any): any {
    let tempArray;
    arrayQuit.forEach(element => {
        arrayBase.forEach(element2 => {

            if (element.name === element2.name) {
                tempArray = this.removeItem(arrayBase, element2);
            }

        });
    });
    return tempArray;
  }

  removeItem(array, item): any {
    const index = array.indexOf(item);
    if (index >= 0) {
        array.splice(index, 1);
    }
    return array;
  }

  removeChip(fruit: any): void {
      const index = this.buildingsSelected.indexOf(fruit);
      if (index >= 0) {
          this.buildingsSelected.splice(index, 1);
          this.buildingsList.push(fruit);
      }

  }

  selectedRouter(item): any {
      const index = this.buildingsList.indexOf(item);
      if (index >= 0) {
          this.buildingsList.splice(index, 1);
      }
      this.buildingsSelected.push(item);
  }

  // saveBuildingsStaff(): void {
  //   this._staffService.saveBuildingsStaff(this.buildingsSelected)
  //     .subscribe((res: any) => {
  //       this._staffService.showSanckBar('¡Datos Guardados!');
  //     }, (err: any) => {
  //       this._staffService.showSanckBar('¡Upss! ' + err.errors);
  //     });
    
  // }


}
