import { Component, OnInit, Inject } from '@angular/core';
import { StaffService } from '../staff.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BuildingsService } from '../../buildings/buildings.service';

@Component({
  selector: 'app-set-building-staff',
  templateUrl: './set-building-staff.component.html',
  styleUrls: ['./set-building-staff.component.scss']
})
export class SetBuildingStaffComponent implements OnInit {
  buildingsList: any;
  id_staff: any;
  angForm: FormGroup;
  constructor(private _staffService: StaffService,
              private _buildingsService: BuildingsService,
              public dialogRef: MatDialogRef<SetBuildingStaffComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public _data) {
                this.id_staff = _data.id;
                this.createForm();
                this.getBuildingsList();
                this.getBuildingStaff();
               }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      building: ['', Validators.required ]
    });
  }

  getBuildingsList(): void {
    this._buildingsService.getListBuildings()
      .subscribe((res: any) => {
        this.buildingsList = res;
      }, (err: any) => {
        this._staffService.showMessageExcep(err);
      });
  }

  getBuildingStaff(): void {
    this._staffService.getBuildingStaff(this.id_staff)
    .subscribe((res: any) => {
      this.angForm.controls['building'].setValue(res.edificio_id);
     }, (err: any) => {
      this._staffService.showMessageExcep(err);
    });
  }

  saveBuildingStaff(): void {
    this._staffService.saveBuildingStaff(this.id_staff, this.angForm)
    .subscribe((res: any) => {
      this._staffService.showSanckBar('¡Datos Guardados!');
     }, (err: any) => {
      this._staffService.showMessageExcep(err);
    });
  }

}
