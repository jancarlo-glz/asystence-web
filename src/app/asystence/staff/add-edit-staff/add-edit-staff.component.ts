import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StaffService } from '../staff.service';

@Component({
  selector: 'app-add-edit-staff',
  templateUrl: './add-edit-staff.component.html',
  styleUrls: ['./add-edit-staff.component.scss']
})
export class AddEditStaffComponent implements OnInit {

  type: any;
  id_staff: any;
  angForm: FormGroup;
  roleList: any = [
    { value: 'Admin', name: 'Administrador' },
    { value: 'Usuario', name: 'Usuario' },
  ];

  constructor(public dialogRef: MatDialogRef<AddEditStaffComponent>,
              private fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public _data,
              private _staffService: StaffService) {
      this.type = _data.type;
      if (_data.id) {
        this.id_staff = _data.id;
        this.getStaffById();
      }
      this.createForm();
     }

  ngOnInit() {
  }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      role: ['', Validators.required],
      email: ['', Validators.required ],
      password: ['', Validators.required ]
    });
  }

  getStaffById(): void {
    this._staffService.getStaffById(this.id_staff)
      .subscribe((res: any) => {
        this.angForm.controls.name.setValue(res.nombre);
        this.angForm.controls.role.setValue(res.rol);
        this.angForm.controls.email.setValue(res.email);
        this.angForm.controls.password.setValue(res.password);
      }, (err: any) => {
        this._staffService.showMessageExcep(err);
      });
  }

  addStaff(): void {
    this._staffService.saveStaff(this.angForm)
      .subscribe((res: any) => {
        this._staffService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._staffService.showMessageExcep(err);
      });
  }

  editStaff(): void {
    this._staffService.editStaff(this.id_staff, this.angForm)
      .subscribe((res: any) => {
        this._staffService.showSanckBar('¡Datos Agregados!');
        this.dialogRef.close();
      }, (err: any) => {
        this._staffService.showMessageExcep(err);
      });
  }


}
