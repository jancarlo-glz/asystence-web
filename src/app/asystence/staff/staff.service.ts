import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MessageExceptionsComponent } from '../message-exceptions/message-exceptions.component';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  url = 'http://18.222.150.29/api/';
  // url = 'http://192.168.10.129/api/';
  constructor(private http: HttpClient,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) { }

    getAllStaff(): any {
      return this.http.get(this.url + 'personal');
    }

    getStaffById(id): any {
      return this.http.get(this.url + 'personal/' + id + '/edit');
    }

    saveStaff(formData): any {
      const body  = {
        nombre: formData.value.name,
        tipo: formData.value.role,
        email: formData.value.email,
        password: formData.value.password
      };
      return this.http.post(this.url + 'personal/store', body);
    }

    editStaff(id, formData): any {
      const body  = {
        nombre: formData.value.name,
        tipo: formData.value.role,
        email: formData.value.email,
        password: formData.value.password
      };
      return this.http.put(this.url + 'personal/' + id + '/update', body);
    }

    saveBuildingStaff(id, formData): any {
      const body = {
        id_edificio: formData.value.building
      };
      return this.http.put(this.url + 'personal/' + id + '/update/edificio', body);
    }

    getBuildingStaff(id): any {
      return this.http.get(this.url + 'personal/' + id + '/edificio');
    }

    deleteStaff(id): any {
      return this.http.delete(this.url + 'personal/' + id + '/destroy');
    }

    showSanckBar(message): void {
      this.snackBar.open(message, 'OK', {
        duration: 3000
      });
    }

    showMessageExcep(message): void {
      if (!message.error.isTrusted) {
        const dialogRef = this.dialog.open(MessageExceptionsComponent, {
          width: '450px',
          data: {
            message: message.error
          }
        });
      }
    }
}
