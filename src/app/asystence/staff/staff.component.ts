import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { ConfirmDeletedComponent } from '../confirm-deleted/confirm-deleted.component';
import { BuildingsService } from '../buildings/buildings.service';
import { StaffService } from './staff.service';
import { AddEditStaffComponent } from './add-edit-staff/add-edit-staff.component';
import { SetBuildingStaffComponent } from './set-building-staff/set-building-staff.component';

export interface StaffData {
  id: string;
  nombre: string;
  rol: string;
  email: string;
  password: string;
}

/** Datos de Prueba para ayudar a mostrar informacion dentro de la tabla */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  displayedColumns: string[] = ['name', 'role', 'email', 'options'];
  dataSource: MatTableDataSource<StaffData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              private _buildingService: BuildingsService,
              private _staffService: StaffService) {
    // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
    this.getAllStaff();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.getAllStaff();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllStaff(): void {
    this._staffService.getAllStaff()
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }, (err: any) => {
        this._staffService.showMessageExcep(err);
      });
  }

  addStaff(): void {
    const dialogRef = this.dialog.open(AddEditStaffComponent, {
      width: '650px',
      data: {
        type: 'add'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllStaff();
    });
  }

  editStaff(id): void {
    const dialogRef = this.dialog.open(AddEditStaffComponent, {
      width: '650px',
      data: {
        type: 'edit',
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllStaff();
    });
  }

  setBuildingStaff(id): void {
    const dialogRef = this.dialog.open(SetBuildingStaffComponent, {
      width: '450px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllStaff();
    });
  }

  deleteStaff(id): void {
    const dialogRef = this.dialog.open(ConfirmDeletedComponent, {
      width: '250px',
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._staffService.deleteStaff(id)
          .subscribe(arg => {
            this._staffService.showSanckBar('¡Datos Eliminados!');
            this.getAllStaff();
          }, (err: any) => {
            this._staffService.showMessageExcep(err);
          });
      }
    });
  }

}

/** Builds and returns a new User. */
function createNewUser(id: number): StaffData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    nombre: Math.round(Math.random() * 100).toString(),
    rol: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    email: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
    password: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
  };
}

