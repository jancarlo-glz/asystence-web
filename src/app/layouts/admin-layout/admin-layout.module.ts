import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';
import { MatIconModule, MatPaginatorModule, MatTableModule,
         MatSortModule, MatFormFieldModule, MatInputModule,
         MatSelectModule, MatRippleModule, MatButtonModule,
         MatTooltipModule, MatDatepickerModule, MatNativeDateModule,
         MatSnackBarModule, MatDialogModule, MatBadgeModule, MatChipsModule,
         MatToolbarModule, MatListModule, MatCheckboxModule, MatRadioModule, MatExpansionModule } from '@angular/material';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BuildingsComponent } from 'src/app/asystence/buildings/buildings.component';
import { AddEditBuildingsComponent } from 'src/app/asystence/buildings/add-edit-buildings/add-edit-buildings.component';
import { ConfirmDeletedComponent } from 'src/app/asystence/confirm-deleted/confirm-deleted.component';
import { ClassroomByBuildingsComponent } from 'src/app/asystence/buildings/classroom-by-buildings/classroom-by-buildings.component';
// tslint:disable-next-line: max-line-length
import { AddEditClassroomComponent } from 'src/app/asystence/buildings/classroom-by-buildings/add-edit-classroom/add-edit-classroom.component';
import { StaffComponent } from 'src/app/asystence/staff/staff.component';
import { AddEditStaffComponent } from 'src/app/asystence/staff/add-edit-staff/add-edit-staff.component';
import { SetBuildingStaffComponent } from 'src/app/asystence/staff/set-building-staff/set-building-staff.component';
import { AddEditSemesterComponent } from 'src/app/asystence/semester/add-edit-semester/add-edit-semester.component';
import { SemesterComponent } from 'src/app/asystence/semester/semester.component';
import { SubjectsComponent } from 'src/app/asystence/subjects/subjects.component';
import { AddEditSubjectsComponent } from 'src/app/asystence/subjects/add-edit-subjects/add-edit-subjects.component';
import { TeachersComponent } from 'src/app/asystence/teachers/teachers.component';
import { AddEditTeachersComponent } from 'src/app/asystence/teachers/add-edit-teachers/add-edit-teachers.component';
import { ShowSubjectsComponent } from 'src/app/asystence/subjects/show-subjects/show-subjects.component';
import { SelectionModel } from '@angular/cdk/collections';
import { QuestionnairesComponent } from 'src/app/asystence/questionnaires/questionnaires.component';
import { AddEditQuestionnariesComponent } from 'src/app/asystence/questionnaires/add-edit-questionnaries/add-edit-questionnaries.component';
import { QuestionsComponent } from 'src/app/asystence/questions/questions.component';
import { ScheduleComponent } from 'src/app/asystence/schedule/schedule.component';
import { AddEditScheduleComponent } from 'src/app/asystence/schedule/add-edit-schedule/add-edit-schedule.component';
import { AsystenceComponent } from 'src/app/asystence/asystence.component';
import { AnswersComponent } from 'src/app/asystence/answers/answers.component';
import { ShowAnswersComponent } from 'src/app/asystence/answers/show-answers/show-answers.component';
import { MessageExceptionsComponent } from 'src/app/asystence/message-exceptions/message-exceptions.component';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatListModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatDialogModule,
    MatBadgeModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
  ],
  declarations: [
    DashboardComponent,
    BuildingsComponent,
    AddEditBuildingsComponent,
    ConfirmDeletedComponent,
    ClassroomByBuildingsComponent,
    AddEditClassroomComponent,
    StaffComponent,
    AddEditStaffComponent,
    SetBuildingStaffComponent,
    AddEditSemesterComponent,
    SemesterComponent,
    SubjectsComponent,
    AddEditSubjectsComponent,
    TeachersComponent,
    AddEditTeachersComponent,
    QuestionnairesComponent,
    QuestionsComponent,
    AddEditQuestionnariesComponent,
    ShowSubjectsComponent,
    ScheduleComponent,
    AddEditScheduleComponent,
    AsystenceComponent,
    UserProfileComponent,
    AnswersComponent,
    ShowAnswersComponent,
    // MessageExceptionsComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent
  ],
  entryComponents: [
    AddEditBuildingsComponent,
    ConfirmDeletedComponent,
    AddEditClassroomComponent,
    AddEditStaffComponent,
    SetBuildingStaffComponent,
    AddEditSemesterComponent,
    AddEditSubjectsComponent,
    ShowSubjectsComponent,
    AddEditQuestionnariesComponent 
  ]
})

export class AdminLayoutModule {}
