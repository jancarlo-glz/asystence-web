import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { BuildingsComponent } from 'src/app/asystence/buildings/buildings.component';
import { ClassroomByBuildingsComponent } from 'src/app/asystence/buildings/classroom-by-buildings/classroom-by-buildings.component';
import { StaffComponent } from 'src/app/asystence/staff/staff.component';
import { SemesterComponent } from 'src/app/asystence/semester/semester.component';
import { SubjectsService } from 'src/app/asystence/subjects/subjects.service';
import { SubjectsComponent } from 'src/app/asystence/subjects/subjects.component';
import { TeachersComponent } from 'src/app/asystence/teachers/teachers.component';
import { AddEditTeachersComponent } from 'src/app/asystence/teachers/add-edit-teachers/add-edit-teachers.component';
import { AuthGuard } from 'src/app/asystence/_guards/auth.guard';
import { QuestionsComponent } from 'src/app/asystence/questions/questions.component';
import { QuestionnairesComponent } from 'src/app/asystence/questionnaires/questionnaires.component';
import { AddEditScheduleComponent } from 'src/app/asystence/schedule/add-edit-schedule/add-edit-schedule.component';
import { ScheduleComponent } from 'src/app/asystence/schedule/schedule.component';
import { AnswersComponent } from 'src/app/asystence/answers/answers.component';
import { ShowAnswersComponent } from 'src/app/asystence/answers/show-answers/show-answers.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },

    { path: 'buildings',      component: BuildingsComponent, canActivate: [AuthGuard] },
    { path: 'building/classsroom/:id',      component: ClassroomByBuildingsComponent, canActivate: [AuthGuard] },
    { path: 'staff',      component: StaffComponent, canActivate: [AuthGuard] },
    { path: 'semester',      component: SemesterComponent, canActivate: [AuthGuard] },
    { path: 'subjects',      component: SubjectsComponent },
    { path: 'teacher',      component: TeachersComponent, canActivate: [AuthGuard] },
    { path: 'teacher/:type',      component: AddEditTeachersComponent, canActivate: [AuthGuard] },
    { path: 'teacher/:type/:id',      component: AddEditTeachersComponent, canActivate: [AuthGuard] },
    { path: 'questionnaries',      component: QuestionnairesComponent, canActivate: [AuthGuard] },
    { path: 'questionnaries/questions/:id',      component: QuestionsComponent, canActivate: [AuthGuard] },
    { path: 'schedule',      component: ScheduleComponent, canActivate: [AuthGuard] },
    { path: 'schedule/:type',      component: AddEditScheduleComponent, canActivate: [AuthGuard] },
    { path: 'schedule/:type/:id',      component: AddEditScheduleComponent, canActivate: [AuthGuard] },
    { path: 'answers',      component: AnswersComponent, canActivate: [AuthGuard] },
    { path: 'answers/show/:id',      component: ShowAnswersComponent, canActivate: [AuthGuard] },

];
