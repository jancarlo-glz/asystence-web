'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">asystence-web documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminLayoutModule.html" data-type="entity-link">AdminLayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AdminLayoutModule-b7ab5bc3b5895693073ae3b0d10c68ca"' : 'data-target="#xs-components-links-module-AdminLayoutModule-b7ab5bc3b5895693073ae3b0d10c68ca"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminLayoutModule-b7ab5bc3b5895693073ae3b0d10c68ca"' :
                                            'id="xs-components-links-module-AdminLayoutModule-b7ab5bc3b5895693073ae3b0d10c68ca"' }>
                                            <li class="link">
                                                <a href="components/AddEditBuildingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditBuildingsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditClassroomComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditClassroomComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditQuestionnariesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditQuestionnariesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditScheduleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditScheduleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditSemesterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditSemesterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditStaffComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditStaffComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditSubjectsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditSubjectsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddEditTeachersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddEditTeachersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AnswersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AnswersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AsystenceComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AsystenceComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BuildingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BuildingsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ClassroomByBuildingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ClassroomByBuildingsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConfirmDeletedComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmDeletedComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IconsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">IconsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MapsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MapsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuestionnairesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuestionnairesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuestionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuestionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScheduleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScheduleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SemesterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SemesterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SetBuildingStaffComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SetBuildingStaffComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShowAnswersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ShowAnswersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShowSubjectsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ShowSubjectsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StaffComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StaffComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SubjectsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SubjectsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TablesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TablesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TeachersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TeachersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserProfileComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' : 'data-target="#xs-components-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' :
                                            'id="xs-components-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' }>
                                            <li class="link">
                                                <a href="components/AdminLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AdminLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AuthLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessageExceptionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessageExceptionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SetSubjectsTeacherComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SetSubjectsTeacherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' : 'data-target="#xs-injectables-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' :
                                        'id="xs-injectables-links-module-AppModule-7d1cc298cf93546e88d8a1b61490e105"' }>
                                        <li class="link">
                                            <a href="injectables/AnswersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AnswersService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/BuildingsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>BuildingsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ClassroomService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ClassroomService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LoginService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LoginService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/QuestionnariesService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>QuestionnariesService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/QuestionsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>QuestionsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ScheduleService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ScheduleService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SemesterService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SemesterService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StaffService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StaffService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SubjectsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SubjectsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TeachersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TeachersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthLayoutModule.html" data-type="entity-link">AuthLayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthLayoutModule-e3ed711d3e8b57d783cccf4345c4e372"' : 'data-target="#xs-components-links-module-AuthLayoutModule-e3ed711d3e8b57d783cccf4345c4e372"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthLayoutModule-e3ed711d3e8b57d783cccf4345c4e372"' :
                                            'id="xs-components-links-module-AuthLayoutModule-e3ed711d3e8b57d783cccf4345c4e372"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ComponentsModule.html" data-type="entity-link">ComponentsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ComponentsModule-29a3149a94bb3866f535d9a97d6272b1"' : 'data-target="#xs-components-links-module-ComponentsModule-29a3149a94bb3866f535d9a97d6272b1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComponentsModule-29a3149a94bb3866f535d9a97d6272b1"' :
                                            'id="xs-components-links-module-ComponentsModule-29a3149a94bb3866f535d9a97d6272b1"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SidebarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SidebarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AsystenceService.html" data-type="entity-link">AsystenceService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/RouteInfo.html" data-type="entity-link">RouteInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StaffData.html" data-type="entity-link">StaffData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StaffData-1.html" data-type="entity-link">StaffData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StaffData-2.html" data-type="entity-link">StaffData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TeacherData.html" data-type="entity-link">TeacherData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TeacherData-1.html" data-type="entity-link">TeacherData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserData.html" data-type="entity-link">UserData</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});